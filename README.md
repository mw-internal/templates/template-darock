# Da Rock

Darock Template free Microweber CMS theme ready for use. 

Darock Template is great for web portfolio, bloggers or small shop owners. 

This template is flexible and can be customized with ease. It contains 6 main pages, has a full blog and e-commerce support. You can use is for multipurpose website template.

## Features:

* Premade HomePage
* +8 Page Templates (Home, Services, Portfolio, Blog ... others)
* eCommerce Support  (Shop Page, Products Feed, Shopping Cart and Checkout Page)
* Pixel Perfect Design
* Fully Responsive Layout
* User-Friendly Code
* Clean Markup
* Creative Design
* Fully Responsive
* Cross Browser Support
* Easy to Customize
* Well Documented
* Free Lifetime Updates
* Community support